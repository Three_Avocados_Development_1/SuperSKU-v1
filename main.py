# SuperSKU is licensed on the MIT License. See LICENSE.txt for details.
# Version 2.0 (Beta)

# Note: This code along with its other [.py] files are formatted with the Black Formatter.
############################################################ Code Starts Below! ########################################################
# Importing calctools module for calculation functions
import calctools

# Importing messagebox and simpledialog from tkinter for GUI dialog boxes
from tkinter import messagebox, simpledialog

# Importing everything from tkinter for GUI creation
from tkinter import *
import suw  # setup wizard module


class management:
    pCodes = [1, 2, 3]  # SKU's
    # [below] Please place the same values as the pcodes, But instead of the [1,2,3] format, use the ['1','2','3'] format.
    pCodes2 = ["1", "2", "3"]
    # [below] The products name, Aligned to the index of the SKU.
    pObj = ["Sample1", "Sample2", "Sample3"]
    # [below] The products cost, Aligned to the index of the SKU. Make sure to keep all values as Integers!
    pCost = [1, 2, 3]
    # [Below] The scanned objects SKU. For accurate values please go and leave this blank, or else the system may calculate wrong.
    objScanned = []  # Leave Blank, str, list
    objTax = []  # Leave Blank, str, list
    objFinal = []  # Leave Blank, str, list
    objNameScanned = []  # Leave Blank, str, list
    objFinal2 = 0  # leave at zero., int only, var
    objPrice = []
    # management password. DO NOT SET AS "exit" - Errors WILL occur.
    mgmtPassword = "CHANGE"
    mgmtPasswordEnabled = False  # enable the management password
    employees = ["Username"]
    empPasswords = ["Password"]
    loggedinas = None

    class app:
        def inttsSetup():
            suw.Setup(
                management.employees,
                management.empPasswords,
                management.mgmtPasswordEnabled,
                management.mgmtPassword,
            )
            earoot.update()
            earoot.update_idletasks()

        class EmployeeAgent:
            def logout():
                mr.destroy()
                global loggedinas
                management.loggedinas = None
                global kroot
                kroot = Tk()
                kroot.title("**REGISTER CLOSED**")
                kroot.geometry("500x500")
                l1 = Label(kroot, text="**REGISTER CLOSED**")
                b1 = Button(
                    kroot,
                    text="Open Register",
                    command=management.app.EmployeeAgent.lop,
                )
                l1.pack()
                b1.pack()

            def lop():
                kroot.destroy()
                if management.mgmtPasswordEnabled == True:
                    while True:
                        pswrd = simpledialog.askstring(
                            "**OPENING REGISTER",
                            "This register is currently closed. To open it again, Please enter the management password.",
                        )
                        if pswrd == management.mgmtPassword:
                            break
                        else:
                            messagebox.showerror(
                                "**OPENING REGISTER / FAILURE",
                                "Failed to open register: Wrong Password!",
                            )
                management.app.EmployeeAgent.employeeLogon()

            def employeeLogon():
                global earoot
                global ename
                global epass
                earoot = Tk()
                earoot.title("Employee Logon")
                earoot.geometry("1000x1000")
                ename = Entry(earoot, text="Username")
                epass = Entry(earoot, text="Password")
                t1 = Label(earoot, text="Username")
                t2 = Label(earoot, text="Password")
                t3 = Label(
                    earoot,
                    text="Default username and password:\nUsername\nPassword\n(the setup wizard is NOT recommended. It is only change the srttings for a temporary amount of time.\nWe suggest editing the functions that are located at the top of the code, see documentation.)",
                )
                t1.pack()
                ename.pack()
                t2.pack()
                epass.pack()
                b1 = Button(
                    earoot, text="Log In", command=management.app.EmployeeAgent.login
                )
                b1.pack()
                t3.pack()
                b2 = Button(text="Open Setup Wizard", command=management.app.inttsSetup)
                b2.pack()
                earoot.mainloop()

            def login():
                usrname1 = ename.get()
                passwrd1 = epass.get()
                if usrname1 in management.employees:
                    if (
                        passwrd1
                        == management.empPasswords[management.employees.index(usrname1)]
                    ):
                        management.loggedinas = usrname1
                        earoot.destroy()
                        gui.enterTax()
                    else:
                        messagebox.showerror(
                            "Error!", "Valid Username, Incorrect password."
                        )
                else:
                    messagebox.showerror("Error!", "Invalid Username")

        ver = "v1.2.1-Beta"
        # Args = "None"
        # upTD = "Not Implemented"

        def start():
            if management.mgmtPasswordEnabled == True:
                while True:
                    passe = simpledialog.askstring(
                        "Management Password",
                        'Please enter the management password.\n(Use "exit" to Exit this window.)',
                    )
                    if passe == management.mgmtPassword:
                        break
                        pass
                    else:
                        if passe == "exit":
                            return
                        else:
                            messagebox.showerror(
                                "Incorrect Password",
                                "Incorrect Password, Please try again.",
                            )
            root = Tk()
            root.title("Management")
            root.geometry("500x500")
            b1 = Button(root, text="Exit Management", command=root.destroy)
            b1.pack()
            b2 = Button(
                root, text="Void ALL Items", command=management.app.ScanAgent.voidall
            )
            b2.pack()
            b3 = Button(
                root, text="Void Specific Item", command=management.app.ScanAgent.vc
            )
            b3.pack()
            root.mainloop()

        def billc():
            global rootz
            rootz = Tk()
            rootz.title("Bill Customer")
            rootz.geometry("500x500")
            l1 = Label(
                rootz,
                text="Bill Customer\nAmount: " + str(round(management.objFinal2, 2)),
            )
            l1.pack()
            b1 = Button(rootz, text="Go Back", command=rootz.destroy)
            b1.pack()
            b2 = Button(
                rootz, text="Complete Order", command=management.app.billc_after
            )
            b2.pack()
            rootz.mainloop()

        def billc_after():
            management.app.ScanAgent.voidall_nmsg()
            rootz.destroy()
            messagebox.showinfo("Completed", "Order Completed! All objects reset.")

        class ScanAgent:
            def vc():
                vi = simpledialog.askinteger(
                    "VOID,",
                    "Please enter the Object Number. (Eg, for the first value/sku, you enter 1.)",
                )
                vis = vi - 1
                management.objScanned.pop(vis)
                management.objFinal.pop(vis)
                management.objNameScanned.pop(vis)
                management.objTax.pop(vis)
                taxvc = calctools.CalcTax(management.objPrice[vis], taxPercent)
                management.objFinal2 -= calctools.CalcTotal_withtax(
                    vis, management.objPrice[vis]
                )
                management.objPrice.pop(vis)
                management.app.ScanAgent.refresh()
                if management.objScanned == []:
                    management.objScanned = []
                    management.objFinal = []
                    management.objNameScanned = []
                    management.objTax = []
                    management.objFinal2 = 0
                    l1["text"] = ""
                    management.app.ScanAgent.refresh()

            def refresh():
                l1["text"] = (
                    "Object Name: \n"
                    + str(management.objNameScanned)
                    + "\n\n"
                    + "Object Price [with tax]: \n"
                    + str(management.objFinal)
                    + "\n\n"
                    + "Object Tax: \n"
                    + str(management.objTax)
                    + "\n\n Object SKU: "
                    + str(management.objScanned)
                    + "\n\n TOTAL: "
                    + str(round(management.objFinal2, 2))
                    + "\n\nObject Price"
                    + str(management.objPrice)
                )
                mr.update()
                mr.update_idletasks()

            def customscan():
                con = simpledialog.askstring(
                    "Custom Scan",
                    "Please enter the name you would like to display on the list of products.",
                )
                coc = simpledialog.askfloat(
                    "Custom Scan", "Please enter the cost of the object."
                )
                management.objScanned.append(con)
                taxOfObj = calctools.CalcTax(coc, taxPercent)
                totalOfObj = calctools.CalcTotal_withtax(taxOfObj, coc)
                management.objFinal.append(str(totalOfObj))
                management.objNameScanned.append(con)
                management.objTax.append(str(taxOfObj))
                management.objFinal2 += round(totalOfObj, 2)
                management.objPrice.append(coc)
                management.app.ScanAgent.refresh()

            def Scan():
                userInput3 = simpledialog.askstring(
                    "Scan - SuperSKU", "Please enter the SKU of the object."
                )
                if userInput3 in management.pCodes2:
                    pass
                else:
                    messagebox.showerror("SKU not found!", "SKU Not Found.")
                userInput = int(userInput3)
                idx = management.pCodes.index(userInput)
                idxPrice = management.pCodes.index(userInput)
                v = management.pObj[idx]
                vPrice = management.pCost[idxPrice]
                taxOfOBJ = calctools.CalcTax(vPrice, taxPercent)
                totalOfOBJ = calctools.CalcTotal_withtax(taxOfOBJ, vPrice)
                management.objScanned.append(userInput3)
                management.objFinal.append(str(totalOfOBJ))
                management.objNameScanned.append(str(v))
                management.objTax.append(str(taxOfOBJ))
                management.objFinal2 += totalOfOBJ
                management.objPrice.append(int(vPrice))
                # print(totalOfOBJ)
                # print(management.objFinal2)
                # messagebox.showinfo("Result",str(totalOfOBJ)+' LIST '+str(management.objFinal))
                # messagebox.showinfo('Result',str(management.objScanned))
                # messagebox.showinfo('Result',str(management.objNameScanned))
                # messagebox.showinfo('Result',str(management.objTax))
                management.app.ScanAgent.refresh()
                # l1["text"] = (
                #     "Object Name: \n"+str(management.objNameScanned)
                #     + "\n\n"
                #     + "Object Price [with tax]: \n"+str(management.objFinal)
                #     + "\n\n"
                #     + "Object Tax: \n"+str(management.objTax)
                #     + "\n\n Object SKU: "+str(management.objScanned)
                #     + "\n\n TOTAL: "+str(round(management.objFinal2, 2))
                # )
                # mr.update()
                # mr.update_idletasks()

            def voidall():
                yn = messagebox.askyesno(
                    "Void All Items", " Are you sure you would like to Void ALL items?"
                )
                if yn == True:
                    management.objScanned = []
                    management.objFinal = []
                    management.objNameScanned = []
                    management.objTax = []
                    management.objFinal2 = 0
                    l1["text"] = ""
                    management.app.ScanAgent.refresh()
                    messagebox.showwarning(
                        "Result", "All objects voided. Please scan an item."
                    )

            def voidall_nmsg():
                management.objScanned = []
                management.objFinal = []
                management.objNameScanned = []
                management.objTax = []
                management.objFinal2 = 0
                l1["text"] = ""
                mr.update()
                mr.update_idletasks()

        class DiscountAgent:
            def discAmt():
                if management.objFinal2 == 0:
                    messagebox.showerror("Cannot Discount!", "Total is zero!")
                else:
                    daui = simpledialog.askfloat(
                        "Amount Discount",
                        "Please enter the amount you'd like to subtract from the total.",
                    )
                    if float(daui) > management.objFinal2:
                        messagebox.showerror(
                            "Cannot Discount!", "Discount is larger than total!"
                        )
                    else:
                        management.objFinal2 -= daui
                        management.objNameScanned.append(
                            "**DISCOUNT: " + str(daui) + "**"
                        )
                        l1["text"] = (
                            "Object Name: \n"
                            + str(management.objNameScanned)
                            + "\n\n"
                            + "Object [FINAL] Price: \n"
                            + str(management.objFinal)
                            + "\n\n"
                            + "Object Tax: \n"
                            + str(management.objTax)
                            + "\n\n Object SKU: "
                            + str(management.objScanned)
                            + "\n\n TOTAL: "
                            + str(round(management.objFinal2, 2))  # reinstater
                        )
                    mr.update()
                    mr.update_idletasks()


class gui:
    def splash():
        root = Tk()
        root.geometry("500x500")
        root.title("SuperSKU")
        l1 = Label(
            root,
            text="\nSuperSKU "
            + management.app.ver
            + "\n2024 Three Avocodos Development",
        )
        l1.pack()
        root.after(1500, root.destroy)
        root.mainloop()

    def start():
        global mr
        global l1
        mr = Tk()
        mr.title("SuperSKU")
        mr.attributes("-fullscreen", True)
        b9 = Button(
            mr, text="Custom Object Scan", command=management.app.ScanAgent.customscan
        )
        b1 = Button(mr, text="Exit session", command=quit)
        b1.pack()
        b2 = Button(mr, text="Management", command=management.app.start)
        b2.pack()
        b3 = Button(mr, text="Scan A SKU", command=management.app.ScanAgent.Scan)
        b3.pack()
        b4 = Button(mr, text="About this release", command=gui.about)
        b5 = Button(mr, text="Help", command=gui.help)
        l1 = Label(mr, text="")
        l1.pack()
        b6 = Button(mr, text="Discounts", command=management.app.DiscountAgent.discAmt)
        b4.pack()
        b5.pack()
        b6.pack()
        b9.pack()
        b8 = Button(mr, text="Finish Order", command=management.app.billc)
        b8.pack()
        l2 = Label(mr, text="You are currently logged in as: " + management.loggedinas)
        l2.pack()
        b7 = Button(
            mr, text="Close Register", command=management.app.EmployeeAgent.logout
        )
        b7.pack()

        mr.mainloop()

    def enterTax():  # the formula for tax is Tax = amount * (taxPercent/100). to get total (for one obj) it is Amount + Tax
        while True:
            userinput = simpledialog.askstring(
                "Configuration",
                "Please enter your states tax percentage.\n If there is no tax, please enter 0. enter exit to exit",
            )
            if userinput == "exit":
                quit()
            # if type(userinput) is not float:
            #     messagebox.showinfo(
            #         "Information - Exiting",
            #         'Enter 9371.372 to exit the program.',
            #     )
            # gui.enterTax()
            global taxPercent
            try:
                taxPercent = float(userinput)
                break
            except:
                messagebox.showerror(
                    "Error!",
                    "The value you have entered is not a number! Please try again...",
                )
        gui.start()

    def about():
        root = Tk()
        root.geometry("1000x1000")
        root.title("About SuperSKU")
        l1 = Label(root, text="----About----")
        l1.pack()
        l2 = Label(root, text="Version: " + management.app.ver)
        l2.pack()
        l3 = Label(
            root,
            text="""\nSuperSKU Copyright 2024 Three Avocados Development

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.""",
        )
        l3.pack()
        # l4 = Label(root, text="Arguments: " + management.app.Args) #future?
        # l4.pack()
        root.mainloop()

    def help():
        root = Tk()
        root.title("Help (Built-In)")
        root.geometry("1250x1250")
        l1 = Label(root, text="Help\n--------------------")
        l1.pack()
        l2 = Label(
            root,
            text="Not Implemented!",
        )
        l2.pack()
        root.mainloop()


gui.splash()  # splash screen
management.app.EmployeeAgent.employeeLogon()
