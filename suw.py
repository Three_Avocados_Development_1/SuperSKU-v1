# SuperSKU Setup Wizard
"""SUPERSKU SETUP WIZARD"""
from tkinter import messagebox, simpledialog


def Setup(cUsernamesL, cPasswordsL, mgmtpe, mgmtpass):
    print("[suw / setup / main / message / enterance] entering setup wizard")
    c = messagebox.askyesno(
        "Setup",
        "You are entering SETUP mode. In this period, you will be able to go and configure the many different aspects of supersku, Such as configured SKUS and other details.",
    )
    if c == False:
        quit("Exited- Setup Configuration Error - user")
    else:
        upSetup(cUsernamesL, cPasswordsL)
        mgmtPswdSetup(mgmtpe, mgmtpass)
        messagebox.showinfo(
            "Security Notice",
            "You have completed the setup wizard. Due to security reasons, To configure Skus, You must edit the code in main.py See documentaiton for details.",
        )


def mgmtPswdSetup(pe, passv):
    confE = messagebox.askyesno(
        "MgmtSetup", "Do you wish to have a management password enabled?"
    )
    if confE == False:
        pass
    else:
        pe[:] = confE
        confP = simpledialog.askstring(
            "MgmtSetup", "What would you wish for the management password to be?"
        )
        passv[:] = confP
    print("[suw / mgmt] job complete")


def upSetup(ul, pl):
    confU = simpledialog.askstring(
        "Username Conf",
        "Please enter the values of the Usernames.\n Layout: user1,user2\nseperate values in commas. Spaces are not required.",
    )
    confP = simpledialog.askstring(
        "Password Conf",
        "Please enter the values of the Passwords.\n Layout: pass1,pass2\nseperate values in commas. Spaces are not required",
    )
    expConfU = confU.split(",")
    expConfP = confP.split(",")
    ul[:] = expConfU
    pl[:] = expConfP
    print("[suw / user:pass] job complete")
